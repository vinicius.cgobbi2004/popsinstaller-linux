#!/bin/env bash
PATH_INSTALL=$HOME/Documentos/POPSINSTALLER
PATH_CONVERTER=$HOME/Documentos/TO_CONVERT

# Criando os paths
printf "Criando Diretórios"
sleep 2
mkdir $PATH_INSTALL $PATH_INSTALL/APPS $PATH_INSTALL/POPS
clear

GAME_CUE=$(cd $PATH_CONVERTER && find -regextype posix-egrep -regex '.*\.(cue)' | sed -n '1,1p' | awk '{gsub("./", ""); print}')
GAME_BIN=$(cd $PATH_CONVERTER && find -regextype posix-egrep -regex '.*\.(cue)' | sed -n '1,1p' | awk '{gsub("./", ""); gsub(".cue", ".bin"); print}')

# Unindo as tracks
GAME_NAME=$(cd $HOME/Documentos/TO_CONVERT && find -regextype posix-egrep -regex '.*\.(cue)' | sed -n '1,1p' | awk '{gsub("./", ""); gsub(".cue", ""); print}')

mkdir $PATH_CONVERTER/tmp
./Tools/binmerge/binmerge $PATH_CONVERTER/*cue "$GAME_NAME" -o $PATH_CONVERTER/tmp

#Removendo arquivos antigos:
rm $PATH_CONVERTER/*bin $PATH_CONVERTER/*cue

#Movendo novos arquivos:
mv $PATH_CONVERTER/tmp/* $PATH_CONVERTER/
rm -rf $PATH_CONVERTER/tmp
sleep 2
#Obtendo game ID
GAME_ISO=$(cd $PATH_CONVERTER && find -regextype posix-egrep -regex '.*\.(cue)' | sed -n '1,1p' | awk '{gsub("./", ""); gsub(".cue", ".iso"); print}')
ccd2iso $PATH_CONVERTER/"$GAME_BIN" $PATH_CONVERTER/"$GAME_ISO"
7z x $PATH_CONVERTER/"$GAME_ISO" -o$PATH_CONVERTER/tmp
GAME_ID=$(cat $PATH_CONVERTER/tmp/SYSTEM.CNF | grep "BOOT" | sed -n '1,1p' | sed -e "s/\<BOOT = cdrom\>//g"| sed -e "s/\;1//g" | tr -d ':\' | sed 's/\r$//')

printf "Game ID: $GAME_ID"
sleep 5
rm -rf $PATH_CONVERTER/tmp
rm "$PATH_CONVERTER/$GAME_ISO"
#Gerando arquivo VCD
GAME_VCD=$(cd $PATH_CONVERTER && find -regextype posix-egrep -regex '.*\.(cue)' | sed -n '1,1p' | awk '{gsub("./", ""); gsub(".cue", ".VCD"); print}')

cue2pops "$PATH_CONVERTER/$GAME_CUE" "$PATH_INSTALL/POPS/$GAME_ID.$GAME_VCD"
clear
printf "Copiando arquivo de BIOS...\n"
cp PopsInstaller/BIOS/* $PATH_INSTALL/POPS
printf "Copiando arquivos do In Game Reset...\n"
cp PopsInstaller/IGR/* $PATH_INSTALL/POPS
printf "Copiando arquivo POPS_IOX.PAK...\n"
cp PopsInstaller/POPS_IOX/* $PATH_INSTALL/POPS
printf "Gerando arquivos do memory card virtual...\n"
mkdir $PATH_INSTALL/POPS/"$GAME_ID.$GAME_NAME"
cp PopsInstaller/VMC/* $PATH_INSTALL/POPS/"$GAME_ID.$GAME_NAME/"
printf "Gerando arquivos para a aba apps do OPL\n"
mkdir $PATH_INSTALL/APPS/"$GAME_NAME"
cp PopsInstaller/POPS_VER/POPSTARTER.ELF $PATH_INSTALL/APPS/"$GAME_NAME"/"XX.$GAME_ID.$GAME_NAME.ELF"
touch $PATH_INSTALL/APPS/"$GAME_NAME"/title.cfg
printf "title=$GAME_NAME\nboot=XX.$GAME_ID.$GAME_NAME.ELF" >> $PATH_INSTALL/APPS/"$GAME_NAME"/title.cfg
